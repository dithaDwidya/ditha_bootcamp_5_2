<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\event;
use App\event_detail;

class EventController extends Controller
{
    function getDataDetails()
        {

            $event = DB::table('events')
            ->join('event_details', 'id_event', '=', 'event_details.id_event')
            ->select('events.nama_event', 'events.lokasi', 'events.jadwal', 'event_details.tiket', 'event_details.harga')
            ->get();
        }

    function addDataEvents(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); //untuk membungkus transaksi, fungsinya adalah jika insert data sukses maka data 
            // akan dicommit dan disave ke database, dan jika gagal maka data akan dirollback

            try
                {
                    
                    $this->validate($request, [
                    'nama_event' => 'required|max:12',
                    'lokasi' => 'required|max:255'
                    ]);
                    //save ke database
                    $nama_event = $request->input('nama_event'); //data diambil dari client ke server
                    $lokasi = $request->input('lokasi');
                    $jadwal = $request->input('jadwal');

                    // save ke database menggunakan metode eloquen
                    $event = new event;
                    $event->nama_event = $nama_event;
                    $event->lokasi = $lokasi;
                    $event->jadwal = $jadwal;
                    $event->save();

                     $events = event::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($events, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

    function addDataDetails(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); //untuk membungkus transaksi, fungsinya adalah jika insert data sukses maka data 
            // akan dicommit dan disave ke database, dan jika gagal maka data akan dirollback

            try
                {
                    
                    $this->validate($request, [
                    'id_event' => 'required|max:12',
                    'tiket' => 'required|max:255',
                    'harga' => 'required|max:15',
        ]);
                    //save ke database
                    $id_event = $request->input('id_event'); //data diambil dari client ke server
                    $tiket = $request->input('tiket');
                    $harga = $request->input('harga');

                    // save ke database menggunakan metode eloquen
                    $detail = new event_detail;
                    $detail->id_event = $id_event;
                    $detail->tiket = $tiket;
                    $detail->harga = $harga;
                    $detail->save();

                     $details = event_detail::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($details, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

    
}
