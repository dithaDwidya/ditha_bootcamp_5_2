<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'list'], function(){
    Route::get('/','EventController@getDataDetails');
    Route::post('/add','EventController@addDataDetails');

});

Route::group(['prefix' => 'event'], function(){
    Route::post('/add','EventController@addDataEvents');

});