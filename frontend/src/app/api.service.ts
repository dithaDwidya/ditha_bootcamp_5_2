import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class APIService {

  constructor(private http:Http, private router:Router) { }

  getEvent(){
    
    return this.http
    .get('http://localhost:8000/api/list')

    .map(result => result.json()) //map untuk mengconvert json ke object
    .catch(error => Observable.throw(error.json().error) || "Server Error");  //catch untuk menghendle error
    // .subscribe(result => this.userList = result);
  }

  addEvent(obj: Object){

    // let dengan var sama saja cuma let cakupannya lebih kecil
    let body = JSON.stringify(obj); //convert object ke json
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    // this.http.post('https://jsonplaceholder.typicode.com/users', body, options)
    this.http.post('http://localhost:8000/api/event/add', body, options)
    .subscribe(result => console.log(result.json()));

  }

  addDetails(obj: Object){

    // let dengan var sama saja cuma let cakupannya lebih kecil
    let body = JSON.stringify(obj); //convert object ke json
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    // this.http.post('https://jsonplaceholder.typicode.com/users', body, options)
    this.http.post('http://localhost:8000/api/list/add', body, options)
    .subscribe(result => console.log(result.json()));

  }
}
