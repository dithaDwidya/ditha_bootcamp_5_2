import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventMasterComponent } from './event-master/event-master.component';

import { APIService } from './api.service';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventMasterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', component: EventListComponent },
      { path: 'add', component: EventMasterComponent }
  ])
  ],
  providers: [APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
