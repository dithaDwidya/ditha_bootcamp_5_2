import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  eventList: Object[];
  events: Object[];

  

  constructor(private api:APIService) { }

  ngOnInit() {
    this.api.getEvent()
    .subscribe(result => this.eventList = result);
  }

  
  

  // getTotal():number{
  //   var total = 0;
  //   this.eventList.forEach(order => {
  //     total += (events["tiket"] * events["price"]);
  //   });
    
  //   return total;
  // }

}
