import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

    newevent:string = "";
  newlokasi:string = "";
  newjadwal:string = "";

  constructor(private api:APIService) { }

  ngOnInit() {
  }
  
addEvent()
    {
      this.api.addEvent(this.newevent, this.newlokasi, this.newjadwal );
              // .subscribe(result => this.eventlist = result); //untuk mereload halaman pada saat add data

      this.newevent = "";
      this.newlokasi = "";
      this.newjadwal = "";
           
      // this.api.getUserList()
                // .subscribe(result => this.userlist = result);
    }  

}
